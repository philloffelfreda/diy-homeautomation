
= Contributing to diy-homeautomation

:description: In diesem Dokument wird beschrieben wie zu diy-homeautomation beigetragen werden kann.


== Overview
{description}



== Vorbereitungen
Bevor du zum Projekt beitragen kannst solltest du dich zunächst mit dem Projekt vertraut machen. Sieh dir dazu link:getting-Started.adoc[getting-Started] an.

Um am Code zu arbeiten solltest du dir zunächst eine Fork des Repository erstellen. Bist du dann mit deinen änderungen Fertig kannst du dein Feature pushen. Für weitere Infos schau di den link:https://blog.seibert-media.net/blog/2014/04/25/git-workflows-der-forking-workflow-teil-2/[Forking-Workflow] an.

== Implementieren einer neuen Funktion
Willst du eine neue Funktion implementieren solltest du dir zunächst überlegen, ob du nur den UniversalNode erweiterst oder ob du zusätzlich einen ganz neuen Code anlegen musst.

=== Erweitern des universalNode
In den meisten fällen kannst du deine Funktion vollständig als Erweiterung des UniversalNodes implementieren. Falls dies nicht der Fall ist, un du z.B. eine neue Platine mit eigener Software benötigst, musst du trotzdem den universalNode Erweitern.

Dazu gehst du bitte wie folgt vor:

==== Implementieren der Konfiguration
Lege eine Option zum einbinden deiner Funktion in der link:node/firmware/universalNode/config.h[config.h] Datei an. Dabei musst du direkt die DeviceID deiner Funktion definieren. Sollte es noch andere Optionen geben die vor dem Installieren getroffen werden müssen legst du diese auch direkt in der config.h Datei an. Einstellungen die normalerweise nicht vom Nutzer verändert werden sollen legst du bitte in der link:node/firmware/universalNode/config_board.h[config_board.h] Datei an.

Die änderung in der config.h Datei sieht dan ungefähr so aus.

[source,cpp]
----
//Deine Funktion
#define DEINE_FUNKTION <DeviceID>
#ifdef DEINE_FUNKTION
  #define <Option_1>
  #define <Option_2>
#endif
----

==== Erstellen der Funktion
Für deine Funktion legst du eine neue Klasse an,
deine Funktion ist entweder ein Sensor, also ein Datensammler, oder ein Aktor.
Du solltest deine Klassen-Deklaratio und -Definiton etnsprechend bennenen:

* sensorDeineFunktion.h
* sensorDeineFunktion.cpp

oder 

* aktorDeineFunktion.h
* aktorDeineFunktion.cpp

Diese Klasse erbt dann von der Device Klasse, eine Minimale Klasse sieht dann wie folgt aus:


.sensorDeineFunktion.h
[source,cpp]
----
#pragma once

#include "device.h"
#include "config.h"
#include "config_board.h"

class SensorDeineFunktion : public Device
{
public:
    SensorDeineFunktion(int aDeviceID);
    ~SensorDeineFunktion();
    void handle(Payload *message = NULL); 
};
----

.sensorDeineFunktion.cpp
[source,cpp]
----
#include "config.h"
#ifdef DEINE_FUNKTION
#include "sensorTemsensorDeineKlasse.h"
#include "config_board.h"
#include "radio.h"

SensorTempHum::SensorTempHum(int aDeviceID): Device(aDeviceID)
{
  //Hier Initialisierst du deine Klasse
    this->handle();
    return;
}

void SensorTempHum::handle(Payload *message)
{
  //Hier kommt das auslesen deines Sensors
  //oder das schalten deines Aktors hin
  radio->send(DEINE_FUNKTION,<value>);
  return;
}
#endif
----

Falls du eine Insanz einer weiteren Klasse benötigst oder weitere Methoden anlegen musst, kannst du das gerne tun.
Jedoch sollte alles aus der handle Methode aufgerufen werden.

==== Anlegen im Hauptprogramm
Hast du deine Klasse erstellt  musst du sie nurnoch im UniversalNode anlegen. Dazu musst du sie wie folgt einbinden:
[source,cpp]
----
#include"sensorDeineFunktion.h"
----

und in der setup() Funktion ein Objekt anlegen:
[source,cpp]
----
#ifdef DEINE_FUNKTION
  new SensorDeineFunktion(&radio,DEINE_FUNKTION);
#endif
----


=== Anlegen von neuen Sketches und PCBs
Falls du weiteren Code benötigst, der nicht auf dem Standart Sensor läuft legst du dir bitte eine Ordnerstruktur nach dem Folgenden Muster an.

```
diy-homeautomation  
│
└───sensoren
|    |
|    └───<Dein-Funktion>
|    |   |
|    │   └───firmware
|    │   |   │   <DeineFunktion>.ino
|    │   |   │   ...
|    |   |
|    │   └───pcb
|    |   |
|    │   └───test

```
