EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ATtiny Slave Platine"
Date "2021-01-08"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20PU U1
U 1 1 5FF89704
P 5100 2850
F 0 "U1" H 4571 2896 50  0000 R CNN
F 1 "ATtiny85-20PU" H 4571 2805 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 5100 2850 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 5100 2850 50  0001 C CNN
	1    5100 2850
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:MA04-1 SV1
U 1 1 5FF8A873
P 3550 3750
F 0 "SV1" H 3683 4136 59  0000 C CNN
F 1 "MA04-1" H 3683 4031 59  0000 C CNN
F 2 "Sensornode:MA04-1" H 3550 3750 50  0001 C CNN
F 3 "" H 3550 3750 50  0001 C CNN
	1    3550 3750
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:MA04-1 SV2
U 1 1 5FF8C1D9
P 3550 4350
F 0 "SV2" H 3472 4247 59  0000 R CNN
F 1 "MA04-1" H 3472 4352 59  0000 R CNN
F 2 "Sensornode:MA04-1" H 3550 4350 50  0001 C CNN
F 3 "" H 3550 4350 50  0001 C CNN
	1    3550 4350
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+01
U 1 1 5FF9BCEF
P 4050 3950
F 0 "#P+01" H 4050 3950 50  0001 C CNN
F 1 "+5V" V 4050 4018 59  0000 L CNN
F 2 "" H 4050 3950 50  0001 C CNN
F 3 "" H 4050 3950 50  0001 C CNN
	1    4050 3950
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+02
U 1 1 5FF9D787
P 4050 4550
F 0 "#P+02" H 4050 4550 50  0001 C CNN
F 1 "+5V" V 4050 4618 59  0000 L CNN
F 2 "" H 4050 4550 50  0001 C CNN
F 3 "" H 4050 4550 50  0001 C CNN
	1    4050 4550
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+03
U 1 1 5FF9E03B
P 5900 2850
F 0 "#P+03" H 5900 2850 50  0001 C CNN
F 1 "+5V" V 5900 2918 59  0000 L CNN
F 2 "" H 5900 2850 50  0001 C CNN
F 3 "" H 5900 2850 50  0001 C CNN
	1    5900 2850
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND01
U 1 1 5FF9E686
P 4400 3000
F 0 "#GND01" H 4400 3000 50  0001 C CNN
F 1 "GND" H 4400 2879 59  0000 C CNN
F 2 "" H 4400 3000 50  0001 C CNN
F 3 "" H 4400 3000 50  0001 C CNN
	1    4400 3000
	1    0    0    -1  
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND02
U 1 1 5FF9ED1B
P 4400 3850
F 0 "#GND02" H 4400 3850 50  0001 C CNN
F 1 "GND" V 4400 3782 59  0000 R CNN
F 2 "" H 4400 3850 50  0001 C CNN
F 3 "" H 4400 3850 50  0001 C CNN
	1    4400 3850
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:GND #GND03
U 1 1 5FF9F855
P 4400 4450
F 0 "#GND03" H 4400 4450 50  0001 C CNN
F 1 "GND" V 4400 4382 59  0000 R CNN
F 2 "" H 4400 4450 50  0001 C CNN
F 3 "" H 4400 4450 50  0001 C CNN
	1    4400 4450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5400 3750 3850 3750
Wire Wire Line
	3850 4350 5400 4350
Wire Wire Line
	5400 3450 5400 3750
Connection ~ 5400 3750
Wire Wire Line
	5400 3750 5400 4350
Wire Wire Line
	5200 3450 5200 3650
Wire Wire Line
	5200 3650 3850 3650
Wire Wire Line
	5200 3650 5200 4250
Wire Wire Line
	5200 4250 3850 4250
Connection ~ 5200 3650
Wire Wire Line
	3850 3950 3950 3950
Wire Wire Line
	3850 3850 4300 3850
Wire Wire Line
	3850 4550 3950 4550
Wire Wire Line
	3850 4450 4300 4450
Wire Wire Line
	5300 3450 5300 4100
Wire Wire Line
	5300 4100 5900 4100
Wire Wire Line
	5950 4000 6300 4000
Wire Wire Line
	6200 3900 6300 3900
$Comp
L Sensornode-eagle-import:GND #GND04
U 1 1 5FFA174A
P 5850 4000
F 0 "#GND04" H 5850 4000 50  0001 C CNN
F 1 "GND" V 5850 3931 59  0000 R CNN
F 2 "" H 5850 4000 50  0001 C CNN
F 3 "" H 5850 4000 50  0001 C CNN
	1    5850 4000
	0    1    1    0   
$EndComp
$Comp
L Sensornode-eagle-import:+5V #P+04
U 1 1 5FFA044F
P 6100 3900
F 0 "#P+04" H 6100 3900 50  0001 C CNN
F 1 "+5V" V 6100 3969 59  0000 L CNN
F 2 "" H 6100 3900 50  0001 C CNN
F 3 "" H 6100 3900 50  0001 C CNN
	1    6100 3900
	0    -1   -1   0   
$EndComp
$Comp
L Sensornode-eagle-import:MA03-1 SV3
U 1 1 5FF8E8E8
P 6600 4000
F 0 "SV3" V 6786 3772 59  0000 R CNN
F 1 "MA03-1" V 6681 3772 59  0000 R CNN
F 2 "Sensornode:MA03-1" H 6600 4000 50  0001 C CNN
F 3 "" H 6600 4000 50  0001 C CNN
	1    6600 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	5700 2850 5800 2850
Wire Wire Line
	4400 2900 4400 2850
Wire Wire Line
	4400 2850 4500 2850
Text Label 3900 3650 0    50   ~ 0
SCL
Text Label 3900 4250 0    50   ~ 0
SCL
Text Label 3900 3750 0    50   ~ 0
SDA
Text Label 3900 4350 0    50   ~ 0
SDA
Text Label 5700 4100 3    50   ~ 0
PCINT1
Text Notes 3400 4150 2    50   ~ 0
Eingang und Weiterleitung \nSpannungsversorgung und I2C-Bus
Text Notes 7500 4000 2    50   ~ 0
Anschluss Leseköpfe\n
$Comp
L Device:R R1
U 1 1 60BFE14A
P 5900 4300
F 0 "R1" H 5970 4346 50  0000 L CNN
F 1 "R" H 5970 4255 50  0000 L CNN
F 2 "Sensornode:0207_10" V 5830 4300 50  0001 C CNN
F 3 "~" H 5900 4300 50  0001 C CNN
	1    5900 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 4150 5900 4100
Connection ~ 5900 4100
Wire Wire Line
	5900 4100 6300 4100
$Comp
L Sensornode-eagle-import:GND #GND05
U 1 1 60BFF179
P 5650 4500
F 0 "#GND05" H 5650 4500 50  0001 C CNN
F 1 "GND" V 5650 4431 59  0000 R CNN
F 2 "" H 5650 4500 50  0001 C CNN
F 3 "" H 5650 4500 50  0001 C CNN
	1    5650 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	5750 4500 5900 4500
Wire Wire Line
	5900 4500 5900 4450
Text Notes 5950 4500 0    50   ~ 0
Pulldown Widerstand
$EndSCHEMATC
