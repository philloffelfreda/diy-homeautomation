#include <TinyWireS.h>


//------------------------------------------------------------------------------------------------------------------------------------------------
#define I2C_ID 1                //Die ID für die I2C-Slaves sollte von 1 beginnend aufsteigend durchnummeriert werden, sonst kann der Master diese nicht auslesen
#define Zaehler_Startwert 3.59  //Der Startwert kann vom Zähler abgelesen werden
#define Impulse_pro_Einheit 10  //Die Anzahl an Impulsen die der Zähler sendet bis eine Einheit erreicht ist. (Bsp: 1000 Impulse pro KWh)
//------------------------------------------------------------------------------------------------------------------------------------------------



volatile float impulsMeterReading = 0; //variable for the last meter reading
volatile float impulsesPerValue = 0; //variable for rotations per kWh/ m^3 o.ae.
volatile float impulses = 0;  //number of impulses since last sending of meter reading

volatile unsigned long last_interrupt_time = 0;
volatile unsigned long interrupt_time = 0;
int debounceTime = 1;

void setup()
{
  //Löschen des Global Interrupt Enable Bits (I) im Status Register (SREG)
  SREG &= 0x7F; //gleichwertig mit "cli();"

  //Setze des Pin Change Interrupt Enable Bit
  GIMSK |= (1 << PCIE);

  //Setzen des Pin Change Enable Mask Bit 1 (PCINT1)  ==> Pin PB1
  PCMSK |= (1 << PCINT1);

   //Setzen des Global Interrupt Enable Bits (I) im Status Register (SREG)
  SREG |= 0x80; // gleichwertig mit "sei();"

  TinyWireS.begin(I2C_ID); // join i2c network
  TinyWireS.onRequest(Daten_senden);
  
  impulsMeterReading = Zaehler_Startwert;
  impulsesPerValue = Impulse_pro_Einheit;
}

//Aufruf der Interrupt Serviceroutine
ISR(PCINT0_vect)
{
    interrupt_time = millis();
    if(abs(interrupt_time - last_interrupt_time) > debounceTime){
      impulses++;
      last_interrupt_time = interrupt_time; 
    }
}

void Daten_senden()
{
  impulsMeterReading = impulsMeterReading + impulses/(impulsesPerValue*2);
  
  uint64_t data = uint64_t(round(impulsMeterReading*1000));
  byte data_array[8];

  data_array[0] = (data >> 56) & 0xFF;
  data_array[1] = (data >> 48) & 0xFF;
  data_array[2] = (data >> 40) & 0xFF;
  data_array[3] = (data >> 32) & 0xFF;
  data_array[4] = (data >> 24) & 0xFF;
  data_array[5] = (data >> 16) & 0xFF;
  data_array[6] = (data >> 8) & 0xFF;
  data_array[7] = data & 0xFF;
   
   for (int i = 0; i < 8; i++)
   TinyWireS.write(data_array[i]);
    
  impulses = 0;
}


void loop() {
  
}
