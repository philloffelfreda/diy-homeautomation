
//
// HC-SR04 hardware settings
//

// HC-SR04 Trigger/Echo pins
#define HC_SR04_TRIGGER 7
#define HC_SR04_ECHO 6
// HC-SR04 Power-pin and boot-time for powersaving
#define HC_SR04_POWER 5
#define HC_SR04_BOOTTIME_MS 100
