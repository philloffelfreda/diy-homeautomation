#include <HCSR04.h>
#include "config.h"

// 2019-05-08 Manuel Reimer
// Test-Sketch zum Ausprobieren des HC-SR04-Sensors in der Sensorknoten-Platine

UltraSonicDistanceSensor distanceSensor(HC_SR04_TRIGGER, HC_SR04_ECHO);  // Initialize sensor that uses digital pins 13 and 12.

void setup () {
  Serial.begin(9600);
  pinMode(HC_SR04_POWER, OUTPUT);
  digitalWrite(HC_SR04_POWER, HIGH); // PNP-Transistor --> Power invertiert!
}

void loop () {
  // Every 500 miliseconds, do a measurement using the sensor and print the distance in centimeters.

  // Power on sensor
  digitalWrite(HC_SR04_POWER, LOW);
  // Wait for sensor to boot
  delay(HC_SR04_BOOTTIME_MS);
  // Get one measurement
  Serial.println(distanceSensor.measureDistanceCm());
  // Power down sensor
  digitalWrite(HC_SR04_POWER, HIGH);

  delay(500);
}
