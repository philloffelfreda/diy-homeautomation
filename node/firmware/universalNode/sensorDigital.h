#include "config.h"
#ifdef SENSOR_DIGITAL
#pragma once

#include "device.h"
#include "radio.h"

class SensorDigital : public Device
{

public:
    SensorDigital(int aDeviceID);
    ~SensorDigital();
    static void handleInterrupt();
    void handle(Payload *message = NULL);
};
#endif