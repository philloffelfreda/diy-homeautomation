#include "config.h"
#ifdef D0_SMARTMETER
#pragma once

#include "radio.h"
#include "device.h"



class SensorD0 : public Device
{
  private:
  byte  serial_data;
  String inString;
  float   D0_meter_Reading;
  float   D0_current_Power;
  unsigned long time_1;
  unsigned long time_2;

  public:
    SensorD0(int aDeviceID);
    ~SensorD0();
    void handle(Payload *message = NULL);  
};
#endif