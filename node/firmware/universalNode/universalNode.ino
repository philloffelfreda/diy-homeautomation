/*
Original Author: Eric Tsai
Updated by Ingo Haschler
License: CC-BY-SA, https://creativecommons.org/licenses/by-sa/2.0/
Date: 9-1-2014
Original File: UberSensor.ino
This sketch is for a wired Arduino w/ RFM69 wireless transceiver
Sends sensor data (gas/smoke, flame, PIR, noise, temp/humidity) back 
to gateway.  See OpenHAB configuration file.

Required libraries:
    • Adafruit Unified Sensor by Adafruit
    • DHT sensor library by Adafruit
    • RFM69_LowPowerLab by LowPowerLab
    • SPIFlash_LowPowerLab by LowPowerLab
    • Low-Power by Rocket Scream Electronics
    • HCSR04 by gamegine

Blink codes on startup: 1+2+3: everything OK, continous blinking: failed to init RFM module
*/

#include "config.h"
#include "config_board.h"
#include "device.h"

#include <LowPower.h>
#include "utils.h"
#include "radio.h"
#include <SPI.h>
#include "sensorTempHum.h"
#include "sensorDistance.h"
#include "sensorDigital.h"
#include "actorDeko.h"
#include "SensorImpuls.h"
#include "sensorD0.h"
#include "sensorMBus.h"

const int SLEEPTIME_8S = 8;

RFMRadio radio;
boolean skip = true; //only used in MasterSlaveMode




//variables for messuring sending intervall:
unsigned long lastTime;
unsigned long timeNow;
int analog_time_elapsed = 0;

void setup()
{
  // Pin setup
  pinMode(LED, OUTPUT);                      // LED Pin
  pinMode(BATTERY_PIN, INPUT);               // Battey voltage monitor

  // first signal: started :-)
  blinkNTimes(1);
  radio.Init();
  radio.Send(0,0);
  radio.Sleep();
  Device::setRadio(&radio);
//select initialisation of serial interface:
#ifdef MBUS
  new SensorMBus(SENSOR_TEMP_HUM);
#elif D0_SMARTMETER
  new SensorD0(SENSOR_TEMP_HUM);
#elif IMPULS_SENSOR
  new SensorImpuls( IMPULS_SENSOR);
#else
  if (debug)
  {
    Serial.begin(SERIAL_BAUD);
  }
#endif

  //temperature / humidity sensor
#ifdef SENSOR_TEMP_HUM
  new SensorTempHum(SENSOR_TEMP_HUM);
#endif

#ifdef SENSOR_DISTANCE
  new SensorDistance(SENSOR_DISTANCE);
#endif

#ifdef SENSOR_DIGITAL
  new SensorDigital(SENSOR_DIGITAL);
#endif

#ifdef DEKO_ELEMENT
  new ActorDeko(DEKO_ELEMENT);
#endif

  lastTime = millis(); //set start time.
}

void intervallMode()
{

  if (disable_PowerSave)
  { //disabled powersafe mode
    #ifdef IMPULS_SENSOR
      impulsSensor.changeSettings(); //checks serial monitor for setting changes
    #endif
timeNow = millis();
if(abs(timeNow - lastTime) >= (ANALOG_DELAY_SECONDS * 1000)){ //abs() for overflow of millis; *1000 for converting from seconds to milliseconds

  Device::handleListOfDevices();

      lastTime = timeNow;
    }
  }
  else
  { //normal powersafe mode

    if (analog_time_elapsed > ANALOG_DELAY_SECONDS)
    {
      analog_time_elapsed = 0;
      Device::handleListOfDevices();
    }
    else
      debugPrintf("Skipped turn. Elapsed: ", analog_time_elapsed);

    // Enter power down state for 8 s with ADC and BOD module disabled
    delay(200);
    radio.Send(0,0);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    delay(100);

    // Add the 8 seconds we slept above
    analog_time_elapsed += SLEEPTIME_8S;
  }
}

void masterSlaveMode()
{
  Payload incomingMsg;
  if (radio.receiveDone(incomingMsg))
  {
    blinkNTimes(1);
    //for debbuging
    debugPrintf("nodeID = ", incomingMsg.nodeID);
    debugPrintf("deviceID = ", incomingMsg.deviceID);
    debugPrintf("uptime_ms = ", incomingMsg.uptime_ms);
    debugPrintf("sensordata = ", incomingMsg.sensordata);
    debugPrintf("battery_volts = ", incomingMsg.battery_volts);

    Device::handleListOfDevices(&incomingMsg);
    skip = true;
  }

  if (skip)
  {
    radio.listenModeStart();
    debugPrintln("Listen Mode Start");
    //go into sleep mode
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
    debugPrintln("Woke up");
    blinkNTimes(1);
    radio.listenModeEnd();
    skip = false;
  }
}

void loop()
{
  if (enable_SlaveMode)
  {
    masterSlaveMode();
  }
  else
  {
    intervallMode();
  }
} //end loop
