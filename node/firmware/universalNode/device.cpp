#include "device.h"
#include "Array.h"
Array<Device *, MAX_NUMBER_OF_DEVICES> Device::listOfDevices;
Device::Device(int aDeviceID)
{
    deviceID = aDeviceID;
    listOfDevices.push_back(this);
    return;
}

void Device::handle(Payload *message) {}

static void Device::handleListOfDevices(Payload *message)
{
    if (message)
    {
        for (int i = 0; i < listOfDevices.size(); i += 1)
        {

            if (message->deviceID == listOfDevices[i]->deviceID)
            {
                listOfDevices[i]->handle(message);
                return;
            }
        }
    }
    else
    {
        for (int i = 0; i < listOfDevices.size(); i += 1)
        {
            listOfDevices[i]->handle();
            blinkNTimes(1);
        }
        return;
    }
}

static RFMRadio *Device::radio;

void Device::setRadio(RFMRadio *aRadio)
{
    radio = aRadio;
    return;
}
