#include "config.h"
#ifdef SENSOR_DIgital

#include "sensorDigital.h"
#include "utils.h"
#include "config_board.h"
#include "radio.h"

SensorDigital::SensorDigital(int aDeviceID) : Device(aDeviceID)
{
    // Digital sensor is interrupt driven
    pinMode(DIGITAL_SENSOR_PIN, INPUT_PULLUP); // Digital sensor
    attachInterrupt(digitalPinToInterrupt(DIGITAL_SENSOR_PIN), handleInterrupt, FALLING);
    return;
}


void SensorDigital::handle(Payload *message)
{
}

void SensorDigital::handleInterrupt()
{
  radio->Send(SENSOR_DIGITAL, 1111);
  blinkNTimes(1);
  radio->Sleep();
}
#endif
