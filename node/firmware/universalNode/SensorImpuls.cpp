#include "config.h"
#ifdef IMPULS_SENSOR

#include "SensorImpuls.h"
#include "config_board.h"
#include "Arduino.h"
#include "radio.h"
#include <SPI.h>
#include "utils.h"

volatile float SensorImpuls::impulsMeterReading = 0; //variable for the last meter reading
volatile float SensorImpuls::impulsesPerValue = 0; //variable for rotations per kWh/ m^3 o.ae.
volatile float SensorImpuls::impulses = 0;  //number of impulses since last sending of meter reading
volatile unsigned int SensorImpuls::debounceTime = 0; //current set debounce time of interrupt
volatile unsigned long SensorImpuls::last_interrupt_time = 0;
volatile unsigned long SensorImpuls::interrupt_time = 0;

SensorImpuls::SensorImpuls(int aDeviceID) : Device(aDeviceID)
{
    Serial.begin(SERIAL_BAUD);
    //set starting value for ferraris_sensor
    this->impulsMeterReading = START_METER_READING;
    this->impulsesPerValue = impulses_Per_Value;
    this->debounceTime = debounce_Time;
    // Ferraris counter interrupt driven
    attachInterrupt(digitalPinToInterrupt(IMPULS_SENSOR_PIN), this->impulsCounterInterrupt, CHANGE);
}
SensorImpuls::~SensorImpuls() {}

//Interrupt service routine: executed when sensor detects impulse
void SensorImpuls::impulsCounterInterrupt()
{
    interrupt_time = millis();
    if (abs(interrupt_time - last_interrupt_time) > debounceTime)
    { //debounce interrupt with debounceTime
        //increase impulse counter by one:
        impulses++;
        //debug message for serial monitor
        if (debug)
        {
            debugPrintln("interrupt impulse");
        }
        last_interrupt_time = interrupt_time;
    }
}

//function for sending meter value via radio
//parameter: RFMRadio Object
void SensorImpuls::handle(Payload *message = NULL)
{
    //calculate meter reading:
    impulsMeterReading = impulsMeterReading + impulses / (impulsesPerValue * 2);
    //send value of meter:
    radio->Send(IMPULS_SENSOR, impulsMeterReading);
    //send number of impulses since last last sending
    delay(100);
    radio->Send(IMPULS_SENSOR_NUM_IMPULSES, impulses);
    if (debug)
    {
        Serial.println("Impulses");
        Serial.println(impulses);
    }
    //reset impulse counter:
    impulses = 0;
    radio->Sleep();
    //debug message
    debugPrintf("radio_send", impulsMeterReading);
}

//function for changing setting of impulse counter via the serial interface
static void SensorImpuls::changeSettings()
{
    while (Serial.available() > 0)
    {
        //read data from serial interface
        serial_data = Serial.read();
        //check for end of line
        if (serial_data != '\n')
        {
            inString += (char)serial_data;
        }
        else
        { //new line has been send:
            if (debug)
            {
                Serial.println("message arrived:");
                Serial.println(inString);
            }
            //Change impulsesPerValue by sending:  'impulsesPerValue: <Value>'
            if (inString.indexOf("impulsesPerValue:") != -1)
            {
                this->impulsesPerValue = (inString.substring(17)).toFloat();
                if (debug)
                { //debug messages
                    Serial.print("Current impulsesPerValue: ");
                    Serial.println(this->impulsesPerValue);
                }
            }
            //Change impulsMeterReading by sending: 'impulseMeterReading: <Value>'
            if (inString.indexOf("impulseMeterReading:") != -1)
            {
                this->impulsMeterReading = (inString.substring(20)).toFloat();
                if (debug)
                { //debug messages
                    Serial.print("current impulseMeterReading: ");
                    Serial.println(this->impulsMeterReading);
                }
            }
            inString = "";
        }
    }
}

#endif