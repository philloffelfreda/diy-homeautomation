#include "config.h"
#ifdef DEKO_ELEMENT

#include "actorDeko.h"
#include "utils.h"
#include "config_board.h"
#include "radio.h"

ActorDeko::ActorDeko(int aDeviceID) : Device(aDeviceID)
{
    pinMode(DEKO, OUTPUT);
    digitalWrite(DEKO, LOW);
    return;
}

void ActorDeko::handle(Payload *message)
{
    if (message)
    {
        if (message->sensordata == 0)
        {
            digitalWrite(DEKO, HIGH);
            radio->Send(DEKO_ELEMENT, 1);
        }
        if (message->sensordata == 1)
        {
            digitalWrite(DEKO, LOW);
            radio->Send(DEKO_ELEMENT, 0);
        }
    }
    return;
}
#endif