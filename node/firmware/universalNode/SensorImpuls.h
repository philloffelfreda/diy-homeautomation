#include "config.h"
#ifdef IMPULS_SENSOR
#pragma once

#include "radio.h"
#include "device.h"

class SensorImpuls : public Device
{
  private:
    //variable for impulse counter:
    //time variable for debouncing interrupt
    static volatile unsigned long last_interrupt_time;
    static volatile unsigned long interrupt_time;

    //variable for reading serial interface for changing settings
    byte serial_data;     
    String inString ="";


  public:
    //constructor/destructor:
    SensorImpuls(int aDeviceID);
    ~SensorImpuls();

    //functions
    //interrupt service routine, counts number of interrupt
    static void impulsCounterInterrupt();

    //calculates new meter reading form counted impulses, impulsvalue and last meter reading and sends it via radio
    void handle(Payload *message = NULL);

    //function for reading serial interface for input to change settings of impulse Sensor (Serial Interface has to be activated for using this function)
    //send "impulsesPerValue:<value>" for changing the impulses per value and send "impulseMeterReadin:<value>" for changing the current meter reading
    void changeSettings();

    //variables:
    static volatile float impulsMeterReading; //variable for the last meter reading
    static volatile float impulsesPerValue ; //variable for rotations per kWh/ m^3 o.ae.
    static volatile float impulses;  //number of impulses since last sending of meter reading
    static volatile unsigned int debounceTime; //current set debounce time of interrupt

};
#endif