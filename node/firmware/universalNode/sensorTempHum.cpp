#include "config.h"
#ifdef SENSOR_TEMP_HUM
#include "sensorTempHum.h"
#include "utils.h"
#include "config_board.h"
#include "radio.h"

SensorTempHum::SensorTempHum(int aDeviceID) : Device(aDeviceID)
{
    blinkNTimes(3);
    dht->begin();
    this->handle();
    return;
}


void SensorTempHum::handle(Payload *message)
{
  radio->listenModeEnd();
  float h = this->dht->readHumidity();
  float t = this->dht->readTemperature();
  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t))
  {
    debugPrintln("Failed to read from DHT sensor!");
    return;
  }

  debugPrintf("Humidity=", h);
  debugPrintf("   Temp=", t);

  radio->Send(SENSOR_TEMP_HUM, t);
  blinkNTimes(1);
  radio->Send(SENSOR_HUMIDITY, h);
  blinkNTimes(1);
  radio->Sleep();
}
#endif
