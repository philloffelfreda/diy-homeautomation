#include "config.h"
#ifdef MBUS
#pragma once

#include "radio.h"
#include "device.h"

class SensorMBus : public Device
{
private:
    void readSerial(byte message[]);
    bool waitACKSlave();
    void extractZaehlerDaten(byte message[], RFMRadio &radio);
    void initialCommunication(byte address);
    void sendRequest(byte address);
    void wakeUp(int num, int delayMs); //not used in current version

public:
    SensorMBus( int aDeviceID);
    ~SensorMBus();
 
    void handle(Payload *message = NULL);
};
#endif