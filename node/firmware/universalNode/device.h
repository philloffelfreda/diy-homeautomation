#pragma once

#include "utils.h"
#include "radio.h"
#include "Array.h"
const int MAX_NUMBER_OF_DEVICES=100;
class Device
{
public:
  //constructor/destructor
  Device(int aDeviceID);
  ~Device();
  //function calls handlefunction off all or one device
  static void handleListOfDevices(Payload *message = NULL);  
  //List that contains all devices
  static Array <Device*,MAX_NUMBER_OF_DEVICES> listOfDevices;
  static void setRadio(RFMRadio *aRadio);
  //functions that contains what a device is doing
  virtual void handle(Payload *message = NULL);  
protected:
  int deviceID;
  //function that adds a Device to the List
  static void addDevice(Device *newDevice);

  //pointer to the radio class
  static RFMRadio* radio;
};
