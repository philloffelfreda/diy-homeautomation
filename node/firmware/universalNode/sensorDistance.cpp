// Arduino universalNode firmware
// This module handles the communication with an HC_SR04 distance sensor
// It also handles powering up and down the sensor to save battery
#include "config.h"
#ifdef SENSOR_DISTANCE
#pragma once

#include "config_board.h"
#include "Arduino.h"
#include "sensorDistance.h"
#include "utils.h"



// Constructor. Sets up the sensor and initially powers it down
SensorDistance::SensorDistance(int aDeviceID) : Device(aDeviceID)
{
  sensor = new HCSR04(HC_SR04_TRIGGER, HC_SR04_ECHO);

  pinMode(HC_SR04_POWER, OUTPUT);
  digitalWrite(HC_SR04_POWER, HIGH); // PNP-transistor --> Power inverted!
}

// Destructor. Frees used memory.
SensorDistance::~SensorDistance() {
  delete sensor;
}

// This function gets one value from the distance sensor
// Parameter: none
// Return value: Distance in centimeters
void SensorDistance::handle(Payload *message)
{
  // Power on sensor
  digitalWrite(HC_SR04_POWER, LOW);
  // Wait for sensor to boot
  delay(HC_SR04_BOOTTIME_MS);
  // Get one measurement
  double value = sensor->dist();
  // Power down sensor
  digitalWrite(HC_SR04_POWER, HIGH);
  debugPrintf("Distance=", value);
  radio->Send(deviceID,value);
  blinkNTimes(1);
  radio->Sleep();
}
#endif