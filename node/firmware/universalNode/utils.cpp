// Arduino universalNode firmware
// This file contains some helper functions

#include "utils.h"
#include "config.h"
#include "config_board.h"
#include "Arduino.h"

// Blinks the led n times (freqency 1s)
// Parameters:
//   n: How many times to blink LED
// Return value: None
void blinkNTimes(int n) {
  for(int i = 0; i < n; i += 1) {
    digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(1000);              // wait for 1/10 second
    digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
    delay(1000);             // wait for 1/10 second
  }
}

//
// Print to serial in debug mode only
//

// Prints string value to serial console
// Parameters:
//   s: String to print
// Return value: none
void debugPrintln(const char* s) {
  if (debug)
    Serial.println(s);
}

// Prints one string and one double value to serial console
// Parameters:
//   s: String to print
//   x: double value to print
// Return value: none
void debugPrintf(const char* s, double x) {
  if (debug) {
    char buff[256];
    dtostrf(x, 255, 2, buff);
    Serial.print(s);
    Serial.println(buff);
  }
}
